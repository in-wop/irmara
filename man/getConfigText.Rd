% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/getConfigText.R
\name{getConfigText}
\alias{getConfigText}
\title{Get the configuration text to insert below of the graph}
\usage{
getConfigText(climate_sources, periods, ruleset, cfg = rvgest::loadConfig())
}
\arguments{
\item{climate_sources}{\link{character} select one or more source climate}

\item{periods}{\link{character} select periods}

\item{ruleset}{\link{character} select rule set of the reservoir}
}
\description{
Get the configuration text to insert below of the graph
}
