#' Run the Shiny Application
#'
#' @param ... A series of options to be used inside the app.
#'
#' @export
#' @importFrom shiny shinyApp
#' @importFrom golem with_golem_options
run_app <- function(
  ...
) {
  cfg <- loadConfig()
  initDB(cfg)
  updateVolumeDB(cfg)
  system("R -q -e 'rvgest::prefetch_cdf()'", wait = FALSE)
  with_golem_options(
    app = shinyApp(
      ui = app_ui,
      server = app_server
    ),
    golem_opts = list(...)
  )
}
