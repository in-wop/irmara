#' one_objective_focus UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList
#' @importFrom rvgest objectives
mod_one_objective_focus_ui <- function(id){
  ns <- NS(id)
  stations <- unique(rvgest::objectives$station)
  names(stations) <- station_names
  default_station <- get_golem_config("default_station")
  curve <- getListCurves()
  tagList(
    fluidRow(
      column(width = 4,
             selectInput(
               ns("station"),
               "Station",
               stations,
               selected = default_station,
               multiple = FALSE,
               selectize = TRUE,
               width = NULL,
               size = NULL
             )
      ),
      column(width = 4,
             selectInput(
               ns("level"),
               "Flow threshold",
               getLevelLabels(default_station),
               selected = NULL,
               multiple = FALSE,
               selectize = TRUE,
               width = NULL,
               size = NULL
             )
      ),
      column(width = 4,
             shinyWidgets::pickerInput(
               ns("curve"),
               "Curves Objectives",
               choices = curve,
               selected = head(curve, 3),
               options = list(`actions-box` = TRUE),
               multiple = TRUE
             )
      )
    ),
    fluidRow(
      column(width = 4,
             sliderInput(
               ns("probMax"),
               "Maximum risk probability",
               min = 0,
               max = 1,
               value = 0.5,
               step = 0.05
             )
      ),
      column(width = 4,
             sliderInput(
               ns("probLevels"),
               "Number of color classes",
               min = 0,
               max = 10,
               value = 10,
               step = 1
             )
      )
    ),
    fluidRow(column(width = 12, align = "center", actionButton(ns("btn_update"), "Update results"))),
    br(),
    plotOutput(ns("plot")),
    br(),
    htmlOutput(ns("configText"))
  )
}

#' one_objective_focus Server Functions
#'
#' @noRd
#' @importFrom scales percent
#' @importFrom lubridate ymd
#' @importFrom dplyr collect tbl
#' @importFrom utils head

mod_one_objective_focus_server <- function(id, con, climateSources, climatePeriods, ruleset){
  moduleServer( id, function(input, output, session){
    ns <- session$ns
    observeEvent({
      input$station
    }, {
      updateSelectInput(session,
                        inputId = "level",
                        choices = getLevelLabels(input$station),
                        selected = input$level)
    })

    output$plot <- renderPlot({
      rvgest::plotRiskHeatMap4AllLakes(
        x = rvgest::objectives[57, ],
        con = con,
        ruleset = 1,
        id_rcps = rvgest::getDefaultClimate()$id_rcps,
        id_periods = rvgest::getDefaultClimate()$id_periods,
        mmjjs = seq(365),
        curves = rvgest::getItemCurves(
          con = con,
          selected_curves = head(getListCurves(), 3)
        ),
        probMax = 0.5,
        probLevels = 10,
      )
    })

    output$configText <- renderText({
      getConfigText(climate_source = rvgest::getDefaultClimate()$id_rcps, periods = rvgest::getDefaultClimate()$id_periods, ruleset = 1)
    })

    observeEvent({
      input$btn_update
    }, {
      shinyjs::disable("btn_update")
      progress <- Progress$new(session, min=1, max=10)
      on.exit({
        progress$close()
        shinyjs::enable("btn_update")
      })

      progress$set(message = 'Calculation in progress',
                   detail = 'This may take a while...',
                   value = 10)

      objective <- rvgest::objectives[rvgest::objectives$station == input$station &
                                        rvgest::objectives$level == input$level,]


      curves <- rvgest::getItemCurves(con = con, selected_curves = input$curve)
      p <- rvgest::plotRiskHeatMap4AllLakes(x = objective,
                                            con = con,
                                            ruleset = ruleset(),
                                            id_rcps = climateSources(),
                                            id_periods = climatePeriods(),
                                            mmjjs = seq(365),
                                            probMax = input$probMax,
                                            probLevels = input$probLevels,
                                            curves = curves)

      textconfig <- getConfigText(climateSources(), climatePeriods(), ruleset())

      output$plot <- renderPlot({
        p
      })
      output$configText <- renderText({
        textconfig
      })
    })
  })
}



## To be copied in the UI
# mod_one_objective_focus_ui("one_objective_focus_ui_1")

## To be copied in the server
# mod_one_objective_focus_server("one_objective_focus_ui_1")

