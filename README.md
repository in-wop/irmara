# IRMaRA

Interactive Reservoir MAnagement Risk assessment (IRMaRA) is a R Shiny interface providing probability of failure of flood and drought objective at key locations downstream of the 4 lakes regulating the Seine River.

<!-- badges: start -->
[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
<!-- badges: end -->

## Installation

### Requirements

A database of reservoir management risk assessment must have been created with the 'rvgest' package. See documentation of 'rvgest' for further information.

### Local installation steps

Installation of the package from source should be done in 3 steps:

- download sources
- run `roxygen` for generating `NAMESPACE` file and documentation from sources
- install the package

### Download sources

First possibility, clone the repository with git (recommended):

If you have configured a SSH key for gitlab (See: https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html): `git clone git@gitlab-ssh.irstea.fr:in-wop/irmara.git`

Otherwise, use HTTPS: `git clone https://gitlab.irstea.fr/in-wop/irmara.git`

Second possibility, download the source code at https://gitlab.irstea.fr/in-wop/irmara/-/archive/master/irmara-master.zip

### Build and install the package

Open the project file `irmara.Rproj` in RStudio and type:

```
roxygen2::roxygenise()
remotes::install_local()
```

## Usage

The "inst/cdf" folder must be populated by sqlite file(s) with risk data. See 'rvgest' package to produce them.

Run the Shiny interface with:

```r
irmara::run_app()
```

## Code of Conduct

Please note that the irmara project is released with a [Contributor Code of Conduct](https://contributor-covenant.org/version/2/0/CODE_OF_CONDUCT.html). By contributing to this project, you agree to abide by its terms.