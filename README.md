# IRMaRA

Interactive Reservoir MAnagement Risk assessment (IRMaRA) is a R Shiny interface providing probability of failure of flood and drought objective at key locations downstream of the 4 lakes regulating the Seine River.

<!-- badges: start -->
[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
<!-- badges: end -->

## Installation

```r
# install.packages("remotes")
remotes.install_git("https://forgemia.inra.fr/in-wop/irmara.git")
```

## Usage

Run the Shiny interface with:

```r
irmara::run_app()
```

### Create a docker container running Irmara

Download the source of the package and in the root of the repository run:

```bash
docker build -t irmara .
docker run --name docker.irmara.service -p 3838:80 -v irmara-volume:/lib/irmara irmara
```

It's also possible to add environment variable `IRMARA_BRANCH` to use the code of a specific development branch (or tag) on the irmara repository. Example:

```bash
docker run --name docker.irmara.service -p 3838:80 -e IRMARA_BRANCH=27-programmer-les-mises-a-jour-sur-aubes -v irmara-volume:/lib/irmara irmara
```

The container check the new version of Irmara on the repository at each start. So the container only needs to be restarted for installing the last version of **Irmara** and **rvgest**.

### Run the docker container as a systemd service

Create a file named "docker.irmara.service" in the folder `/etc/systemd/system`:

```
[Unit]
Description=Irmara Service
After=docker.service
Requires=docker.service

[Service]
TimeoutStartSec=0
Restart=always
ExecStart=/usr/bin/docker start -a docker.irmara.service
ExecStop=/usr/bin/docker stop docker.irmara.service
[Install]
WantedBy=default.target
```

And reload the systemd configuration: `sudo systemctl daemon-reload`

## Code of Conduct

Please note that the irmara project is released with a [Contributor Code of Conduct](https://contributor-covenant.org/version/2/0/CODE_OF_CONDUCT.html). By contributing to this project, you agree to abide by its terms.
