FROM rocker/shiny:4.2.1
RUN apt-get update \
  && apt-get install -y  \
    git-core \
    libcurl4-openssl-dev \
    libgit2-dev \
    libicu-dev \
    libssl-dev \
    libxml2-dev \
    make \
    pandoc \
    pandoc-citeproc \
    zlib1g-dev \
  && rm -rf /var/lib/apt/lists/*
RUN R -e 'install.packages("remotes")'
ENV R_LIBS=/lib/irmara
RUN mkdir ${R_LIBS}
COPY dev/entrypoint.R entrypoint.R
ENTRYPOINT [ "Rscript", "entrypoint.R" ]
VOLUME ${R_LIBS}
EXPOSE 80
