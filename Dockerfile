FROM rocker/r-ver:4.0.3
RUN apt-get update && apt-get install -y  git-core libcurl4-openssl-dev libgit2-dev libicu-dev libssl-dev libxml2-dev make pandoc pandoc-citeproc zlib1g-dev && rm -rf /var/lib/apt/lists/*
RUN echo "options(repos = c(CRAN = 'https://cran.rstudio.com/'), download.file.method = 'libcurl')" >> /usr/local/lib/R/etc/Rprofile.site
RUN R -e 'install.packages(c("golem", "processx", "DT", "shinydashboardPlus", "RSQLite", "dbplyr"))'
RUN R -e 'install.packages(c("lubridate", "TSstudio", "scales", "shinyjs"))'
RUN R -e 'install.packages(c("rmarkdown"))'
ARG COMMIT_HASH=unknown
RUN R -e 'remotes::install_gitlab("in-wop/rvgest@master", host = "gitlab.irstea.fr")'
RUN R -e 'remotes::install_gitlab("in-wop/sgldatagrabber@master", host = "gitlab.irstea.fr")'
RUN mkdir /build_zone
ADD . /build_zone
WORKDIR /build_zone
RUN R -e 'remotes::install_local(upgrade="never")'
RUN R -e 'rmarkdown::render(irmara:::app_sys("documentation/documentation.Rmd"))'
EXPOSE 80
CMD R -e "options('shiny.port'=80,shiny.host='0.0.0.0');irmara::run_app()"
